package com.ktbi.bakingcake.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.GsonBuilder;
import com.ktbi.bakingcake.R;
import com.ktbi.bakingcake.model.Ingredient;
import com.ktbi.bakingcake.model.Recipe;
import com.ktbi.bakingcake.model.Step;
import com.ktbi.bakingcake.ui.adapter.IngredientsAdapterView;
import com.ktbi.bakingcake.ui.adapter.StepsAdapterView;

import java.util.List;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class RecipeDetailFragment extends Fragment implements StepsAdapterView.OnStepAdapterViewListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public  static final String STRING_RECIPE = "com.ktbi.bakingcake.ui.RecipeDetailFragment.STRING_RECIPE";
    public  static final String TWO_PANE = "com.ktbi.bakingcake.ui.RecipeDetailFragment.TWO_PANE";

    // TODO: Rename and change types of parameters
    private String sRecipe;
    private boolean mTwoPane;

    private static final String STEPS_POSITION = "STEPS_POSITION";
    private int stepsPosition = -1;
    private int currentVisiblePosition;

    private OnFragmentInteractionListener mListener;
    private RecyclerView rvIngredients;
    private static Bundle mBundleRecyclerViewState;
    private final String KEY_RECYCLER_STATE = "recycler_state";
    private String LIST_STATE_KEY = "recipe_detail_state";
    private Parcelable recyclerViewStepsState;
    private RecyclerView.LayoutManager layout_ingredients_manager, layout_steps_manager;
    private Bundle bundle;
    private NestedScrollView mScrollView;


    public RecipeDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sRecipe Parameter 1.
     * @return A new instance of fragment RecipeDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecipeDetailFragment newInstance(String sRecipe, boolean isTwoPane) {
        RecipeDetailFragment fragment = new RecipeDetailFragment();
        Bundle args = new Bundle();
        args.putString(STRING_RECIPE, sRecipe);
        args.putBoolean(TWO_PANE, isTwoPane);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStepAdapterViewSelected(int position) {
        Log.i("isi position", String.valueOf(position));
        stepsPosition = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("ARTICLE_SCROLL_POSITION",
                new int[]{ mScrollView.getScrollX(), mScrollView.getScrollY()});
        outState.putInt(STEPS_POSITION, stepsPosition);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if(savedInstanceState != null)
        {
            final int[] position = savedInstanceState.getIntArray("ARTICLE_SCROLL_POSITION");
            if(position != null) {
                mScrollView.post(new Runnable() {
                    public void run() {
                        mScrollView.scrollTo(position[0], position[1]);
                    }
                });
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(STRING_RECIPE) && getArguments().containsKey(TWO_PANE)) {
            sRecipe = getArguments().getString(STRING_RECIPE);
            mTwoPane = getArguments().getBoolean(TWO_PANE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null){
            stepsPosition = savedInstanceState.getInt(STEPS_POSITION);
        }
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(com.ktbi.bakingcake.R.layout.fragment_recipe_detail, container, false);

        Recipe recipe = new GsonBuilder().create().fromJson(getArguments().getString(STRING_RECIPE), Recipe.class);

        mScrollView = (NestedScrollView) rootView.findViewById(R.id.sv_fragment_recipe_detail);

        List<Ingredient> ingredients = recipe.getIngredients();
        rvIngredients = (RecyclerView) rootView.findViewById(R.id.rv_ingredients);
        layout_ingredients_manager = new LinearLayoutManager(getActivity());
        IngredientsAdapterView ingredientsAdapterView = new IngredientsAdapterView(ingredients);
        rvIngredients.setAdapter(ingredientsAdapterView);
        rvIngredients.setLayoutManager(layout_ingredients_manager);

        List<Step> steps = recipe.getSteps();
        RecyclerView rvSteps = (RecyclerView) rootView.findViewById(R.id.rv_steps);
        StepsAdapterView stepsAdapterView = new StepsAdapterView(steps, mTwoPane, this);
        rvSteps.setAdapter(stepsAdapterView);
        layout_steps_manager = new LinearLayoutManager(getActivity());
        rvSteps.setLayoutManager(layout_steps_manager);

//        if (stepsPosition != -1)
//            rvSteps.smoothScrollToPosition(stepsPosition);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentMasterSelected(int position);
    }

    public class ScrollRecyclerView extends RecyclerView {

        public ScrollRecyclerView(Context context) {
            super(context);
        }

        public ScrollRecyclerView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public ScrollRecyclerView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public int getVerticalScrollOffset() {
            return super.computeVerticalScrollOffset();
        }

        public int getHorizontalScrollOffset() {
            return super.computeHorizontalScrollOffset();
        }
    }
}
