package com.ktbi.bakingcake.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ktbi.bakingcake.model.Step;
import com.ktbi.bakingcake.ui.RecipeStepDescriptionItemFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class RecipeStepDetailPagerAdapter extends FragmentPagerAdapter {

    Context context;
    List<Step> steps;

    public RecipeStepDetailPagerAdapter(FragmentManager fm, Context context, List<Step> steps) {
        super(fm);
        this.context = context;
        this.steps = new ArrayList<>();
        this.steps.addAll(steps);
    }

    @Override
    public Fragment getItem(int position) {
        Step step = steps.get(position);
        String videoURL = step.getVideoURL();
        String description = step.getDescription();
        return RecipeStepDescriptionItemFragment.newInstance(videoURL, description);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Step step = steps.get(position);
        return step.getShortDescription();
    }

    @Override
    public int getCount() {
        return steps.size();
    }
}