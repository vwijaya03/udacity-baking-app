package com.ktbi.bakingcake.ui;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.ktbi.bakingcake.Network.NetworkTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class MainActivity extends AppCompatActivity {

    // The Idling Resource which will be null in production.
    @Nullable
    private NetworkTask mIdlingResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ProgressBar(this));
        initNetwork();
//        MockData.DATA = MockData.CONTOH_DATA;
//        startActivity(new Intent(MainActivity.this, RecipeActivity.class));
    }

    void initNetwork(){
        new NetworkTask().execute();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String data) {
        /* Do something */
        startActivity(new Intent(MainActivity.this, RecipeActivity.class));
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        PreferenceManager.getDefaultSharedPreferences(this).getString("ID", "");
    }

    /**
     * Only called from test, creates and returns a new {@link NetworkTask}.
     */
    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new NetworkTask();
        }
        return mIdlingResource;
    }
}