package com.ktbi.bakingcake.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.GsonBuilder;
import com.ktbi.bakingcake.R;
import com.ktbi.bakingcake.model.Recipe;
import com.ktbi.bakingcake.model.SelectedPosition;
import com.ktbi.bakingcake.model.Step;
import com.ktbi.bakingcake.ui.adapter.RecipeStepDetailPagerAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class RecipeDetailActivity extends AppCompatActivity
        implements RecipeDetailFragment.OnFragmentInteractionListener{

    public static final String RECIPE = "com.ktbi.bakingcake.ui.RecipeDetailActivity.RECIPE";

    private String sRecipe;
    private Recipe recipe;
    private boolean mTwoPane;

    private ViewPager viewPager;
    private RecipeStepDetailPagerAdapter adapter;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);
        if (getIntent() != null && getIntent().hasExtra(RECIPE)){
            sRecipe = getIntent().getStringExtra(RECIPE);
            recipe = new GsonBuilder().create().fromJson(sRecipe, Recipe.class);
        }

        if (findViewById(R.id.view_pager) != null){
            mTwoPane = true;
            initMasterDetail(savedInstanceState, sRecipe, recipe);
        } else {
            mTwoPane = false;
            init(savedInstanceState, sRecipe);
        }
    }

    private void init(Bundle savedInstanceState, String recipe){
        if (savedInstanceState == null){
            RecipeDetailFragment rdf = RecipeDetailFragment.newInstance(recipe, mTwoPane);
            FragmentManager fm=getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment_container, rdf, RecipeFragment.class.getSimpleName());
            ft.commit();
        }
    }

    private void initMasterDetail(Bundle savedInstanceState, String SRecipe, Recipe recipe){
        init(savedInstanceState, sRecipe);
        initViewPager(savedInstanceState, recipe.getSteps());
    }

    private void initViewPager(Bundle savedInstanceState, List<Step> steps){
        if (savedInstanceState == null){
            viewPager = (ViewPager)findViewById(R.id.view_pager);
            adapter = new RecipeStepDetailPagerAdapter(getSupportFragmentManager(), this, steps);
            viewPager.setAdapter(adapter);
        }
    }

    @Override
    public void onFragmentMasterSelected(int position) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SelectedPosition selectedPosition) {
        /* Do something */
        viewPager.setCurrentItem(selectedPosition.getPosition());
    }

}