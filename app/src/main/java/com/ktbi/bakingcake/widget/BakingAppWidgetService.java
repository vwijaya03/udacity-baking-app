package com.ktbi.bakingcake.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class BakingAppWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        BakingAppDataProvider data = new BakingAppDataProvider(this, intent);
        return data;
    }
}