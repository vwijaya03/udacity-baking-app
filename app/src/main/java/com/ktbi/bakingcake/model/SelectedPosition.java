package com.ktbi.bakingcake.model;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class SelectedPosition {
    private int position;

    public SelectedPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}