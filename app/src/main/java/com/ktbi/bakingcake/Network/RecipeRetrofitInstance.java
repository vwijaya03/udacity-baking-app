package com.ktbi.bakingcake.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vikowijaya on 9/17/17.
 */

public class RecipeRetrofitInstance {

    public static final String URL = "http://go.udacity.com/";

    public static Retrofit getInstance(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}
