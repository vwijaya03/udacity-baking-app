package com.ktbi.bakingcake.Network;

import com.ktbi.bakingcake.model.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by vikowijaya on 9/17/17.
 */

public interface IRecipeAccessNetwork {

    @GET("android-baking-app-json")
    Call<List<Recipe>> getAllRecips();
}
